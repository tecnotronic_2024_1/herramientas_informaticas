# Herramientas informáticas

## Instructor: Ing. Franklin Condori

### Introducción a Microsoft Word

[Material consulta](/0.Materiales/Multinivel.pdf)

Microsoft Word es uno de los procesadores de texto más utilizados a nivel mundial, desarrollado por Microsoft. Su versatilidad y amplia gama de funciones lo convierten en una herramienta esencial tanto para profesionales como para estudiantes. Este curso introductorio tiene como objetivo familiarizar a los participantes con las funcionalidades básicas de Microsoft Word, permitiéndoles crear, editar y formatear documentos de manera eficiente.

El curso está diseñado para usuarios principiantes que desean adquirir las competencias necesarias para utilizar Word en su vida académica o profesional. A lo largo de las lecciones, se explorarán aspectos fundamentales como la creación de documentos, la utilización de plantillas, la edición y el formato de texto, la inserción de imágenes y tablas, y la configuración de páginas. Además, se abordarán técnicas para mejorar la presentación visual de los documentos y herramientas para la revisión y corrección de textos.

Al finalizar este curso, los participantes serán capaces de:

1. Crear y gestionar documentos en Microsoft Word.
2. Aplicar formatos de texto y párrafo para mejorar la legibilidad y presentación.
3. Insertar y manipular elementos gráficos y tablas.
4. Utilizar herramientas de revisión y corrección para garantizar la calidad del contenido.
5. Configurar el diseño de página para impresión y distribución digital.

Este curso introductorio es el primer paso para dominar Microsoft Word, sentando las bases para un uso más avanzado y especializado de esta poderosa herramienta de procesamiento de textos.

* [1. Historia APA](/1.Elaborar_formato_APA/1.Historia.md)

* [2. Configuración](/1.Elaborar_formato_APA/2.Configuracion.md)
