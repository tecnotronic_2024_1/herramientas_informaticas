# Configuración de Office Word para hacer un formato APA

## Formato APA de microsoft Word 2024

Para comenzar un trabajo estilo APA, al abrir Microsoft Word en nuestra computadora debemos seleccionar desde el menú superior la pestaña de «Referencias«, seguido busca la sección de «Estilo» y selecciona la opción de «APA«, Genial, ahora podemos comenzar a configurar cada uno de los elementos que requiere el formato APA es sus páginas.

![1](/1.Elaborar_formato_APA/Imagenes/1.png)

## Tamaño de papel

Tamaño de la hoja: Carta (Letter) / papel 21.59 cm x 27.94 cm (8 1/2” x 11”).

![1](/1.Elaborar_formato_APA/Imagenes/2.png)

## Márgenes

Tamaño de margen: en todos los bordes (Superior, inferior, izquierda y derecha), 2.54 cm. (1 pulgada).

![1](/1.Elaborar_formato_APA/Imagenes/3.png)

## Sangría

Posición y tamaño de sangría: El primer renglón de cada párrafo debe contar 5 espacios de sangría, mientras que el resto del texto queda a la izquierda.

![1](/1.Elaborar_formato_APA/Imagenes/4.png)

## Fuente

Tipo y tamaño de letra: Calibri 11, Arial 11, Lucida Sans Unicode 10, Times New Roman 12, Georgia 11 y Computer Modern 10.

El tipo de letra debes clara y sin adornos, por ejemplo, para títulos de imágenes se suele ocupar la fuente Arial o similar, mientras que para el texto de párrafos se puede utilizar la letra Sans Serif o Times New Roman.

![1](/1.Elaborar_formato_APA/Imagenes/5.png)

## Numeración de página

Las páginas deberán tener el número correspondiente de paginación colocado siempre en la esquina superior derecha, comenzando desde la hoja del título o portada.

La numeración incluye desde páginas de listas, tablas, figuras, imágenes hasta páginas de copyright, dedicatoria o prefacio, el formato APA indica en su séptima edición que los números deben ser arábigos.

![1](/1.Elaborar_formato_APA/Imagenes/6.png)

## Encabezados

Una de las actualizaciones de la séptima edición de las normas APA, nos dice que la palabra “Título corto” ya no se incluye en el formato.

El encabezado de página en trabajos escolares solo se coloca el número de página alineado a la derecha con el título a la izquierda.

El encabezado de página en investigaciones profesionales, este se coloca a la izquierda y el número de página a la derecha.

Desde Word, en la sección de “insertar” del menú superior, haz Clic en la pestaña de “encabezado” y elige “en blanco” para que pueda escribir su título general o corto.

![1](/1.Elaborar_formato_APA/Imagenes/7.png)

## Títulos y subtítulos

Las normas APA en su última versión indican que ya se permite 5 niveles de títulos, estos deben estar alineados a la izquierda y se deben diferenciar por su estilo de formato, a excepción del título nivel 1 que va centrado y con el tamaño de letra más grande..

![1](/1.Elaborar_formato_APA/Imagenes/8.png)

## Alineación de párrafos

Las reglas de la séptima edición de las normas APA. Establece que la alineación de los textos debe ser siempre a la izquierda con sangría en la primera línea del párrafo.

![1](/1.Elaborar_formato_APA/Imagenes/9.png)

## Interlineado de párrafo

Todo el contenido tendrá doble espaciado o interlineado, con unas excepciones.

Excepciones al doble espacio (Salto del párrafo adicional):

* PORTADA: Entre el título de la investigación y la información del autor, se agrega por lo menos una línea extra en blanco.
* TABLAS: Dependiendo del estilo o diseño del trabajo en una tabla se usa interlineado simple o sencillo.
* FIGURAS e IMÁGENES: En textos escritos en imágenes o figuras el interlineado puede ser sencillo de 1,5 o doble para el título, anotaciones y el número de imagen.
* NOTAS AL PIE DE PÁGINA: Se puede utilizar una fuente ligeramente más pequeña que el texto con interlineado simple o sencillo.
* ECUACIONES MATEMÁTICAS: Dependiendo del tipo de ecuación, los ajustes deben mostrar una ecuación amable a la vista, aplicando interlineados triples o cuádruples según convenga.

![1](/1.Elaborar_formato_APA/Imagenes/10.png)

## Abreviaturas para referencias

Lo establecido en la 7ma edición de las normas APA, nos sugiere el uso de abreviaciones de algunas palabras en las referencias.

Tipos de abreviaciones

Existen 4 tipos de abreviaciones que podemos utilizar al crear referencias:

1. Abreviatura – Acortar una palabra con menos letras, regularmente se usa el inicio de la palabra o una mezcla del principio, intermedio y final de la palabra. Lleva punto final “.” o barra diagonal “/” (excepciones). Por ejemplo; Doctor “Dr.”, Señor “Sr.”, Calle “C/”.
2. Símbolo – Se utilizan para trabajos específicos de origen científico o técnico, estos símbolos son válidos para todo el mundo y cualquier idioma, su estilo no cambia sin importar el género, número o mayúsculas y no llevan punto final. Por ejemplo; número pi “TT”, Cromo “Cr”, Metro (medida) “m”, Litro “l”.
3. Sigla – Es una abreviación formada por la primera letra de cada palabra relevante que que compone una oración referente a un nombre, se pueden utilizar como acrónimos y se escriben normalmente en mayúsculas. Por ejemplo; ONU – Organización de las Naciones Unidas, OMS – Organización Mundial de la Salud.
4. Acrónimo – Estas se forman con la unión de dos o más palabras o siglas que crean una palabra de pronunciación fonética clara y se transforman en plural. Por ejemplo; docudrama “documental dramático”, OVNIS – Objeto Volador No Identificado.

Tabla de abreviaturas para referencias (Estilo APA 2024)

![1](/1.Elaborar_formato_APA/Imagenes/tabla.png)

Las pautas de las normas APA nos indica las abreviaciones que podemos utilizar.
Hasta aquí tienes las especificaciones claras de cómo debemos configurar la hoja de Word para crear títulos y textos desde la portada hasta la sección final, todas las medidas y parámetros están establecidos en la séptima edición de las normas APA.

Aprendimos a configurar las páginas en Word para comenzar a crear nuestro trabajo en formato APA, el siguiente paso es ver como comienza, cual es el orden de las secciones y elementos estilo APA que debemos incluir en el trabajo.


Realiza el trabajo siguiendo el orden de estructura de un formato APA.


vea el orden de partes: “Estructura – Cómo Hacer un Formato APA”

Descargue este modelo para facilitar la construcción del escrito: “Plantilla Gratis de Estilo APA para Word 2024”

Esperamos que este pequeño tutorial para configurar correctamente las páginas de un formato APA desde Word hayan sido de ayuda, recuerda que este es solo un paso para comenzar nuestro proyecto.